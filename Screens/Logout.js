import React from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useDispatch } from 'react-redux';
import { setPK } from '../Store/walletBTSlice';

function Logout({ navigation }) {
    const dispatch = useDispatch();

    const handleLogout = async () => {
        try {
            await AsyncStorage.removeItem('privateKey');
            // await AsyncStorage.removeItem('walletBTPP');
            dispatch(setPK(null));
            navigation.navigate("Login");
        } catch (error) {
            console.error('Error logging out:', error);
        }
    }

    return (
        <View style={styles.container}>
            <Text style={styles.headerText}>Logout</Text>
            <Button title="Logout" onPress={handleLogout} />
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    headerText: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 20,

    },
});

export default Logout;
