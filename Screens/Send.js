import React, { useState } from "react";
import {
    Text,
    View,
    StyleSheet,
    TextInput,
    Modal,
    FlatList,
    Pressable,
    Alert,
    ActivityIndicator,
} from "react-native";
import { useSelector, useDispatch } from "react-redux";
import truncateEthAddress from "truncate-eth-address";
import { useFocusEffect } from "@react-navigation/native";
import { setAddress, setPK } from "../Store/walletBTSlice";
import { Ionicons } from "@expo/vector-icons";
import { ethers } from "ethers";
const INFURA_SEPOLIA_API =
    "https://sepolia.infura.io/v3/7ad54549da8d491dbe222c2db9bb0088";

function Send() {
    const dispatch = useDispatch();
    const accounts = useSelector(
        (state) => state.walletBT.walletBTAccounts.walletBTAccounts
    ); const address = useSelector((state) =>
        state.walletBT.currentAccountAddress);
    const key = useSelector((state) =>
        state.walletBT.currentAccountPK);
    const [modalVisible, setModalVisible] = useState(false);
    const [balance, setBalance] = useState(0.0);
    const [toAddress, setToAddress] = useState();
    const [sendAmount, setSendAmount] = useState();
    const [sendStatus, setSendStatus] = useState(false);
    useFocusEffect(
        React.useCallback(() => {
            //This time we are not making use of primary key to access the
            balance
            const getBalance = async () => {
                try {
                    const provider = new ethers.providers.JsonRpcProvider(
                        INFURA_SEPOLIA_API
                    );
                    const balance = await provider.getBalance(address);
                    setBalance(ethers.utils.formatEther(balance));
                } catch (error) {
                    alert(error);
                }
            };
            getBalance();
        }, [address])
    );
    async function sendTransaction() {
        setSendStatus(true);
        try {
            const provider = new
                ethers.providers.JsonRpcProvider(INFURA_SEPOLIA_API);
            let gasPrice = await provider.getGasPrice();
            gasPrice = ethers.utils.formatUnits(gasPrice, "wei");
            const wallet = new ethers.Wallet(key, provider);
            const tx = await wallet.sendTransaction({
                to: toAddress,
                value: ethers.utils.parseEther(sendAmount),
                gasPrice: gasPrice,
            });
            const response = await tx.wait();
            if (response.status) {
                Alert.alert(
                    "Transaction Message",
                    `Successfuly send the amount ${sendAmount} to ${truncateEthAddress(
                        toAddress
                    )} with ${response.transactionHash} transaction hash`,
                    [{
                        text: "Done",
                    },
                    ]
                );
            }
        } catch (error) {
            alert(error);
        }
        setToAddress("");
        setSendAmount("");
        setSendStatus(false);
    }
    return (
        <View style={styles.rootContainer}>
            <Modal
                animationType="slide"
                onRequestClose={() => setModalVisible(false)}
                visible={modalVisible}
                transparent={true}
            >
                <View style={styles.modalContainer}>
                    <FlatList
                        ListHeaderComponent={() => (
                            <Text style={styles.modalHeader}>Select Your
                                Account</Text>
                        )}
                        ListFooterComponent={() => (
                            <Text style={styles.modalFooter}>
                                Click on account to select it
                            </Text>
                        )}
                        data={accounts}
                        renderItem={({ item }) => (
                            <Pressable
                                onPress={() => {
                                    dispatch(setAddress(item.name));
                                    dispatch(setPK(item.key));
                                    setModalVisible(false);
                                }}
                            >
                                <Text
                                    style={styles.modalAccountNameLabel}>{item.name}</Text>
                            </Pressable>
                        )}
                        keyExtractor={(item) => item.name}
                    />
                </View>
            </Modal>
            <View style={styles.headerContainer}>
                <Text style={styles.headerTitle}>Sepolia Test Network</Text>
                <Text style={styles.headerTagLine}>Send Tokens</Text>
            </View>
            <View>
                {!address ? (<Text>You don't have any accounts</Text>
                ) : (
                    <View style={styles.body}>
                        {!sendStatus ? (
                            <>
                                <View style={styles.fromAddressContainer}>
                                    <Text style={styles.label}>From:</Text>
                                    <Pressable
                                        onPress={() => setModalVisible(true)}
                                        style={styles.accountButton}
                                    >
                                        <View style={styles.balanceAccountContainer}>
                                            <Text style={{ color: "white" }}>{truncateEthAddress(address)}</Text>
                                            <Text style={{ color: "white" }}>
                                                Balance: {parseFloat(balance).toFixed(5)}
                                                SeopliaETH
                                            </Text>
                                        </View>
                                        <Ionicons
                                            name="ios-chevron-down-circle-sharp"
                                            size={28}
                                            color="white"
                                            style={styles.dropDownIcon}
                                        />
                                    </Pressable>
                                </View>
                                <View style={styles.toAddressContainer}>
                                    <Text style={styles.label}>To:</Text>
                                    <TextInput
                                        placeholder="Recipient Address"
                                        style={styles.input}
                                        value={toAddress}
                                        onChangeText={(enteredAddress) =>
                                            setToAddress(enteredAddress)
                                        }
                                    />
                                </View>
                                <View style={styles.toAddressContainer}>
                                    <Text style={styles.label}>Amount:</Text>
                                    <TextInput
                                        placeholder="Enter amount in ethers"
                                        style={styles.input}
                                        value={sendAmount}
                                        onChangeText={(enteredAmount) =>
                                            setSendAmount(enteredAmount)
                                        }
                                    />
                                </View>
                                <Pressable
                                    style={styles.sendButton}
                                    onPress={() => sendTransaction()}
                                >
                                    <Text style={styles.sendButtonText}>Send</Text>
                                </Pressable>
                            </>) : (
                            <View style={styles.loadingScreen}>
                                <ActivityIndicator size={70} color="#0a61e7" />
                                <Text style={styles.loadingText}>
                                    Wait Please, sending the token. It might take from
                                    few second to few minutes to complete.
                                </Text>
                            </View>
                        )}
                    </View>
                )}
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    rootContainer: {
        flex: 1,
    },
    headerContainer: {
        justifyContent: "center",
        alignItems: "center",
        borderBottomLeftRadius: 30,
        borderBottomRightRadius: 30,
        backgroundColor: "#0a4f95",
        paddingVertical: 20,
    },
    headerTitle: {
        fontSize: 24,
        fontWeight: "bold",
        color: "white",
    },
    headerTagLine: {
        color: "white",
        fontSize: 16,
    },
    body: {
        marginTop: 20,
    },
    fromAddressContainer: {
        marginTop: 10,
        flexDirection: "row",
    },
    toAddressContainer: {
        marginTop: 10,
        flexDirection: "row",
    },
    label: {
        width: "25%",
        fontSize: 18,
        fontWeight: "bold",
        paddingHorizontal: 10,
        textAlign: "right",
        paddingVertical: 10,
    },
    modalAccountNameLabel: {
        marginVertical: 5,
        backgroundColor: "#0a4f95",
        color: "white",
        paddingVertical: 10,
        paddingHorizontal: 5,
        borderRadius: 5
        // borderBottomRightRadius: 20,
    },
    modalHeader: {
        fontSize: 18,
        fontWeight: "bold",
        paddingVertical: 10,
        alignSelf: "center",
    },
    modalFooter: {
        color: "gray",
        marginTop: 10,
        fontSize: 16,
        backgroundColor: "#dfe8f0",
        paddingVertical: 20,
        textAlign: "center",
    },
    modalContainer: {
        backgroundColor: "#a4c5e6",
        justifyContent: "center",
        alignItems: "center",
        marginTop: "50%",
        backgroundColor: "#cadbec",
        paddingVertical: 40
    },
    accountButton: {
        flexDirection: "row",
        backgroundColor: "#0a61e7",
        paddingVertical: 10,
        paddingHorizontal: 20,
        borderRadius: 30,
    },
    dropDownIcon: {
        paddingLeft: 10,
        justifyContent: "center",
        alignItems: "center",
    },
    balanceAccountContainer: {
        justifyContent: "center",
        alignItems: "center",
    },
    input: {
        flex: 1,
        backgroundColor: "white",
        marginRight: 10,
        paddingLeft: 5,
        borderRadius: 5,
    },
    sendButton: {
        marginTop: 30, alignSelf: "center",
        backgroundColor: "#0a61e7",
        borderRadius: 30,
        paddingVertical: 10,
        paddingHorizontal: 30,
    },
    sendButtonText: {
        fontSize: 18,
        color: "white",
    },
    loadingScreen: {
        paddingVertical: 10,
        paddingHorizontal: 20,
    },
    loadingText: {
        alignSelf: "center",
        fontSize: 16,
        color: "gray"
    },
});
export default Send;