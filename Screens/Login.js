import {
    View,
    Text,
    StyleSheet,
    Button,
    Pressable,
    ActivityIndicator,
    TextInput,
    Alert,
} from "react-native";
import { useState } from "react";
import { ethers } from "ethers";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { setAddress, setPK, setWalletBT } from
    "../Store/walletBTSlice";
import { useDispatch } from "react-redux";

function Login({ navigation }) {
    const dispatch = useDispatch();
    const [passPhrase, setPassPhrase] = useState();
    const [mnemonics, setMnemonics] = useState([]);


    const [isLoading, setIsLoading] = useState(false);
    const [createAccountState, setCreateAccountState] = useState(false);
    const [login, setLogin] = useState(false);
    const [passPhraseInput, setPassPhraseInput] = useState('');

    const guideLines = [
        "1. Click on generate passphrase",
        "2. Copy the passphrase that you generate. It can be used to login later on or to recover your account",
        "3. Click on Create Account to create Account in our wallet",
        "4. In case if you want different passphrase click on Clear and repeat from step 1",
        "5. If you already have an account then use the login button ",
    ];

    function generateMnemonic() {
        setIsLoading(true);
        setTimeout(async () => {
            const passPhrase = ethers.Wallet.createRandom().mnemonic.phrase;
            const mnemonicsString = await AsyncStorage.getItem('walletBTPP');
            let mnemonics = JSON.parse(mnemonicsString) || [];
            mnemonics.push(passPhrase)
            setPassPhrase(passPhrase);
            setMnemonics(mnemonics);
            setIsLoading(false);
        }, 500);
    }

    const handleLogin = async () => {
        if (!passPhraseInput) {
            Alert.alert("Enter Passphrase", "Please enter your generated passphrase to log in to your account");
            return;
        }
        try {
            const mnemonicsString = await AsyncStorage.getItem('walletBTPP');
            const mnemonics = JSON.parse(mnemonicsString) || [];
            if (mnemonics.includes(passPhraseInput)) {
                Alert.alert("Logging in", "This may take some time so please remember to be patient")
                const path = `m/44'/60'/0'/0/0`;
                const hdNode = ethers.utils.HDNode.fromMnemonic(passPhraseInput);
                const myAccount = hdNode.derivePath(path);
                const walletBT = {
                    index: 0,
                    walletBTAccounts: [
                        { name: myAccount.address, key: myAccount.privateKey },
                    ],
                };

                await AsyncStorage.setItem("walletBTAccounts", JSON.stringify(walletBT));
                navigation.navigate("SecondaryNavigator");
            } else {
                Alert.alert("Error!", "Incorrect passphrase. Please try again.");
            }
        } catch (error) {
            console.error('Error logging in:', error);
            Alert.alert("Error!", "An error occurred. Please try again later.");
        }
    };


    function createHDWallet() {
        // State for account create load
        Alert.alert("Creating Account...", "Once your account is created, you will automatically be logged in with your newly created account.")
        setCreateAccountState(true);
        const path = `m/44'/60'/0'/0/0`;
        const hdNode = ethers.utils.HDNode.fromMnemonic(passPhrase);
        //hdNode has the xPub,xPriv, and many more information. You can console log hdNode to see the it
        const myAccount = hdNode.derivePath(path);
        const walletBT = {
            index: 0,
            walletBTAccounts: [
                { name: myAccount.address, key: myAccount.privateKey },
            ],
        };
        setAccountsLocal(walletBT);
        dispatch(setWalletBT(walletBT));
        dispatch(setAddress(myAccount.address));
        dispatch(setPK(myAccount.privateKey));
        setCreateAccountState(false);
        navigation.navigate("SecondaryNavigator");
    }
    // Basically we are storing the accounts addresses inside our mobile storage
    // So that next time we can retrieve from it and i dont't have to login time and again
    async function setAccountsLocal(walletBT) {
        try {
            const accounts = JSON.stringify(walletBT);
            await AsyncStorage.setItem("walletBTPP", JSON.stringify(mnemonics));
            await AsyncStorage.setItem("walletBTAccounts", accounts);
            // await AsyncStorage.setItem("walletBTPP", passPhrase);
        } catch (e) {
            alert(e);
        }
    }
    return (
        <View style={styles.rootContainer}>
            {!createAccountState ? (
                <>
                    <View style={styles.header}>
                        <Text style={styles.title}>Welcome to walletBT</Text>
                        {!login ? (
                            <Text style={styles.tagLine}>
                                Follow the steps given below to create an account
                            </Text>
                        ) : null}
                    </View>
                    {!login ? (
                        <View style={styles.body}>
                            {guideLines.map((step) => (
                                <View key={step} style={styles.guideLinesContainer}>
                                    <Text style={styles.guideLinesText}>{step}</Text>
                                </View>
                            ))}
                            <View style={styles.generatePassPhrase}>
                                <Pressable onPress={() => {
                                    Alert.alert("Generating Mnemonic Phrase...", "Remember to be patient. This might take some time so do not close the app. A loader will be shown until your new mnemonic is generated.")
                                    generateMnemonic();
                                }}>
                                    <Text
                                        style={styles.buttonText}>Generate Mnemonic Phrase</Text>
                                    {isLoading ? (
                                        <ActivityIndicator size="small" color="white" />
                                    ) : null}
                                </Pressable>
                            </View>
                            {/* {passPhrase ? ( */}
                            {passPhrase && (
                                <>
                                    <View style={styles.passPhraseContainer}>
                                        <Text selectable={true} style={styles.mnemonic}>
                                            {passPhrase}
                                        </Text>
                                    </View>
                                    <View style={styles.buttonsAction}>
                                        <View style={styles.button}>
                                            <Button
                                                title="Create Account"
                                                onPress={() => createHDWallet()}
                                            />
                                        </View>
                                        <View style={styles.button}>
                                            <Button title="Clear" onPress={() =>
                                                setPassPhrase("")} />
                                        </View>
                                    </View>
                                </>
                            )}
                            {/* ) : ( */}
                            <View style={styles.loginButtonLink}>
                                <Pressable style={styles.loginBtn} onPress={() => setLogin(true)} >
                                    <Text style={styles.buttonText}>Login</Text>
                                </Pressable>
                            </View>
                            {/* )} */}
                        </View>
                    ) : (
                        <View style={styles.loginContainer}>
                            <Text style={styles.loginLabel}>Enter Your
                                Passphrase:</Text>
                            <TextInput
                                placeholder="Enter your passphrase"
                                style={styles.loginBox}
                                multiline={true}
                                value={passPhraseInput}
                                onChangeText={(input) => setPassPhraseInput(input)}
                            />
                            <View style={styles.loginButton}>
                                <Button
                                    title="Wallet Login"
                                    onPress={() => handleLogin()}
                                />
                            </View>
                            <View style={{ width: 80 }}>
                                <Button
                                    title="Back"
                                    onPress={() => navigation.replace("Login")}
                                />
                            </View>
                        </View>
                    )}
                </>
            ) : (
                <ActivityIndicator size="large" color="white" />
            )}
        </View>
    );
}
export default Login;

const styles = StyleSheet.create({
    rootContainer: {
        flex: 1,
        backgroundColor: "#a4c5e6",
    },
    header: {
        justifyContent: "center",
        alignItems: "center",
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        backgroundColor: "#0a4f95",
        paddingVertical: 20,
    },
    body: {
        marginTop: 5,
    },
    title: {
        fontSize: 24,
        fontWeight: "bold",
        color: "white",
    },
    tagLine: {
        color: "white",
    },
    mnemonic: {
        paddingHorizontal: 2,
        paddingVertical: 5,
        fontSize: 16,
    },
    buttonsAction: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginBottom: 10,
    },
    generatePassPhrase: {
        marginTop: 5,
        marginBottom: 10,
        alignSelf: "center",
        paddingVertical: 10,
        paddingHorizontal: 20,
        backgroundColor: "#2196F3",
        borderRadius: 30,
    },
    buttonText: {
        color: "white",
        fontSize: 16,
        textAlign: "center",
    },
    button: {
        flex: 1,
        marginHorizontal: "5%",
    },
    guideLinesContainer: {
        // backgroundColor: "white",
        margin: 5,
        padding: 10,
        // borderBottomLeftRadius: 20,
        // borderBottomRightRadius: 20,
        alignSelf: "stretch",
    },
    guideLinesText: {
        color: "black",
        fontSize: 18,
    },
    passPhraseContainer: {
        backgroundColor: "#ffffff",
        elevation: 5,
        paddingVertical: 10,
        paddingHorizontal: 5,
        marginBottom: 20,
    },
    loginContainer: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    loginButtonLink: {
        elevation: 5,
        paddingVertical: 10
    },
    loginBtn: {
        alignSelf: "center",
        width: 100,
        borderRadius: 20,
        backgroundColor: "#2196F3",
        padding: 10
    },
    loginBox: {
        backgroundColor: "white",
        alignSelf: "stretch",
        paddingVertical: 10,
        paddingHorizontal: 5,
        marginHorizontal: 20,
        borderRadius: 5
    },
    loginButton: {
        marginTop: 5,
        paddingVertical: 5,
        paddingHorizontal: 10,
    },
    loginLabel: {
        fontSize: 18,
        fontWeight: "bold",
        paddingBottom: 5,
    },
});