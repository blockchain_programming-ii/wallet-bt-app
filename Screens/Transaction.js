import {
    Text,
    View,
    StyleSheet,
    FlatList,
    ActivityIndicator,
    Modal,
    Pressable,
} from "react-native";
import { useSelector, useDispatch } from "react-redux";
import truncateEthAddress from "truncate-eth-address";
import React, { useState } from "react";
import { Ionicons } from "@expo/vector-icons";
import { ethers } from "ethers";
import { useFocusEffect } from "@react-navigation/native";
import { setAddress, setPK } from "../Store/walletBTSlice";
const ETHERSCAN_API_KEY = "DD2KEQK6FNAVFSBU9FUISQ3JMMZIZ2W8GS";

function Transaction() {
    const dispatch = useDispatch();
    const address = useSelector((state) =>
        state.walletBT.currentAccountAddress);
    const accounts = useSelector(
        (state) => state.walletBT.walletBTAccounts.walletBTAccounts
    );
    const [currentAccountAddress, setCurrentAccountAddress] =
        useState(address);
    const [transactionHistory, setTransactionHistory] = useState();
    const [isLoading, setIsLoading] = useState(true);
    const [modalVisible, setModalVisible] = useState(false);
    useFocusEffect(
        React.useCallback(() => {
            setCurrentAccountAddress(address);
            fetch(`https://api-sepolia.etherscan.io/api?module=account&action=txlist&address=${currentAccountAddress}&startblock=0&endblock=99999999&page=1&offset=20&sort=desc&apikey=${ETHERSCAN_API_KEY}`)
                .then((response) => response.json())
                .then((data) => {
                    setTransactionHistory(data.result);
                    setIsLoading(false);
                })
                .catch((error) => alert(error));
        }, [currentAccountAddress, address])
    );
    return (
        <View style={styles.rootContainer}>
            <Modal
                animationType="slide"
                onRequestClose={() => setModalVisible(false)}
                visible={modalVisible}
            >
                <View style={styles.modalContainer}>
                    <FlatList
                        ListHeaderComponent={() => (
                            <Text style={styles.modalHeader}>
                                List of Your Transactions History
                            </Text>
                        )}
                        ListFooterComponent={() => (
                            <Text style={styles.modalFooter}>
                                Click on account to select it
                            </Text>
                        )}
                        data={accounts}
                        renderItem={({ item }) => (
                            <Pressable
                                onPress={() => {
                                    setCurrentAccountAddress(item.name);
                                    dispatch(setAddress(item.name));
                                    dispatch(setPK(item.key));
                                    setModalVisible(false);
                                }}
                            >
                                <Text
                                    style={styles.modalAccountNameLabel}>{item.name}</Text>
                            </Pressable>
                        )}
                        keyExtractor={(item) => item.name}
                    />
                </View>
            </Modal>
            {isLoading ? (
                <View style={styles.loadingContainer}>
                    <ActivityIndicator color="#0a61e7" size={100} />
                    <Text style={styles.loadingText}>Loading the transactions history
                    </Text>
                </View>
            ) : (
                <FlatList
                    ListHeaderComponent={() => (
                        <View style={styles.headerContainer}>
                            <Text style={styles.headerText}>Transaction
                                History</Text>
                            <Pressable
                                style={styles.accountGroupContainer}
                                onPress={() => setModalVisible(true)}
                            >
                                <Text style={{ paddingTop: 5, color: "white" }}>
                                    {truncateEthAddress(currentAccountAddress)}
                                </Text>
                                <Ionicons
                                    name="ios-chevron-down-circle-sharp"
                                    size={28}
                                    color="white"
                                />
                            </Pressable>
                        </View>
                    )}
                    ListFooterComponent={() => (
                        <>
                            {transactionHistory.length > 0 ? null : (
                                <Text style={styles.emptyTransactionText}>
                                    No Transactions Record History Found
                                </Text>
                            )}
                        </>
                    )}
                    data={transactionHistory}
                    keyExtractor={(item) => item.hash}
                    renderItem={({ item, index }) => (
                        //You can make this pressable component and in new screen you can display
                        //All details as well
                        <View style={styles.textCont}>
                            <Text style={styles.txText}>
                                Transaction {index + 1}
                            </Text>
                            <Text style={styles.txText}>
                                Tx Hash: <Text style={styles.custom}>{item.hash}</Text>
                            </Text>
                            <Text style={styles.txText}>
                                Value: <Text style={styles.custom}>
                                    {parseFloat(
                                        ethers.utils.formatUnits(item.value, "ether")
                                    ).toFixed(4)} ETH</Text>
                            </Text>
                            <Text style={styles.txText}>
                                Tx Fee: <Text style={styles.custom}>
                                    {parseFloat(
                                        ethers.utils.formatUnits(
                                            parseInt(item.gas) * parseInt(item.gasPrice),
                                            "ether"
                                        )
                                    ).toFixed(8)} ETH</Text>
                            </Text>
                        </View>
                    )}
                />
            )}</View>
    );
}
const styles = StyleSheet.create({
    rootContainer: {
        flex: 1,
    },
    loadingContainer: {
        justifyContent: "center",
        alignItems: "center",
        flex: 1,
    },
    loadingText: {
        fontSize: 18,
        fontWeight: "bold",
    },
    headerContainer: {
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#0a4f95",
        borderBottomLeftRadius: 30,
        borderBottomRightRadius: 30,
        paddingBottom: 10,
    },
    headerText: {
        fontSize: 20,
        fontWeight: "bold",
        paddingVertical: 5,
        color: "white",
    },
    accountGroupContainer: {
        backgroundColor: "#0a61e7",
        flexDirection: "row",
        paddingVertical: 5,
        paddingHorizontal: 10,
        borderRadius: 30,
    },
    modalAccountNameLabel: {
        marginVertical: 5,
        backgroundColor: "#0a4f95",
        color: "white",
        paddingVertical: 10,
        paddingHorizontal: 5,
        borderBottomRightRadius: 20,
    },
    modalHeader: {
        fontSize: 18,
        fontWeight: "bold",
        paddingVertical: 20,
        alignSelf: "center",
    },
    modalFooter: {
        color: "gray",
        marginVertical: 20,
        fontSize: 16,
        backgroundColor: "#dfe8f0",
        paddingVertical: 20,
        textAlign: "center",
    },
    modalContainer: {
        backgroundColor: "#a4c5e6",
        justifyContent: "center",
        alignItems: "center",
        height: "100%"
    },
    emptyTransactionText: {
        fontSize: 18,
        fontWeight: "bold",
        paddingTop: 20,
        paddingHorizontal: 5,
        textAlign: "center",
        color: "#2a2a2a"
    },
    textCont: {
        backgroundColor: "#0a61e7",
        marginTop: 10,
        paddingVertical: 10,
        paddingLeft: 5,
        marginHorizontal: 5,
    },
    txText: {
        color: "white"
    },
    custom: {
        color: "#d8d8d8"
    }
});
export default Transaction;    