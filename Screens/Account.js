import { useEffect, useState } from "react";
import {
    Text,
    View,
    ActivityIndicator,
    StyleSheet,
    Button,
    Pressable,
    Alert,
    Modal,
    FlatList,
    ScrollView,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { ethers } from "ethers";
import truncateEthAddress from "truncate-eth-address";
import { Ionicons } from "@expo/vector-icons";
import { useDispatch } from "react-redux";
import { setAddress, setPK, setWalletBT } from "../Store/walletBTSlice";
const INFURA_SEPOLIA_API =
    "https://sepolia.infura.io/v3/7ad54549da8d491dbe222c2db9bb0088";
function Account() {
    const dispatch = useDispatch();
    const [walletBTAccounts, setWalletBTAccounts] = useState();
    const [walletBTPP, setWalletBTPP] = useState();
    const [isLoading, setIsLoading] = useState(true);
    const [currentAccountAddress, setCurrentAccountAddress] = useState();
    const [currentAccountPK, setCurrentAccountPK] = useState();
    const [balance, setBalance] = useState(0.0);
    const [keyStatus, setKeyStatus] = useState(false);
    const [createAccountStatus, setCreateAccountStatus] = useState(false);
    const [modalStatus, setModalStatus] = useState(false);
    const [transactionModalVisible, setTransactionModalVisible] = useState(false);
    const [transactionHistory, setTransactionHistory] = useState([]);

    const openTransactionHistoryModal = () => {
        const mockTransactionData = [
            { id: 1, recipient: '0x6190a141a7cEB69162B5a14bbaab8815933c6857', amount: '0.0247 ETH', date: '2023-01-01 10:00 AM' },
            { id: 2, recipient: '0xf1F77446aF46C4a28fa5Ef5976eC24Fe2f5921B5', amount: '1 ETH', date: '2023-01-02 11:30 AM' },
            { id: 3, recipient: '0x6190a141a7cEB69162B5a14bbaab8815933c6857', amount: '0.0247 ETH', date: '2023-01-01 10:00 AM' },
            { id: 4, recipient: '0xf1F77446aF46C4a28fa5Ef5976eC24Fe2f5921B5', amount: '1 ETH', date: '2023-01-02 11:30 AM' },
            { id: 5, recipient: '0x6190a141a7cEB69162B5a14bbaab8815933c6857', amount: '0.0247 ETH', date: '2023-01-01 10:00 AM' },
            { id: 6, recipient: '0xf1F77446aF46C4a28fa5Ef5976eC24Fe2f5921B5', amount: '1 ETH', date: '2023-01-02 11:30 AM' },
            { id: 7, recipient: '0xf1F77446aF46C4a28fa5Ef5976eC24Fe2f5921B5', amount: '1 ETH', date: '2023-01-02 11:30 AM' },
        ];

        setTransactionHistory(mockTransactionData);
        setTransactionModalVisible(true);
    };

    function createAccount() {
        setCreateAccountStatus(true);
        setTimeout(async () => {
            const passPhrase = ethers.Wallet.createRandom().mnemonic.phrase;
            const index = walletBTAccounts.index + 1;
            const path = `m/44'/60'/0'/0/${index}`;
            const hdNode = ethers.utils.HDNode.fromMnemonic(passPhrase);
            const myAccount = hdNode.derivePath(path);
            const walletBT = {
                index: 0,
                walletBTAccounts: [
                    { name: myAccount.address, key: myAccount.privateKey },
                ],
            };
            const mnemonicsString = await AsyncStorage.getItem('walletBTPP');
            let mnemonics = JSON.parse(mnemonicsString) || [];
            mnemonics.push(passPhrase)
            await AsyncStorage.setItem("walletBTPP", JSON.stringify(mnemonics));
            await AsyncStorage.setItem("walletBTAccounts", JSON.stringify(walletBT));

            const existingWalletBTAccounts = walletBTAccounts.walletBTAccounts || [];
            walletBT.walletBTAccounts = [...existingWalletBTAccounts, ...walletBT.walletBTAccounts];

            const updatedWalletBTAccounts = {
                ...walletBTAccounts,
                ...walletBT,
            };
            setWalletBTAccounts(updatedWalletBTAccounts)
            Alert.alert(
                "Account Creation",
                `You have successfully created an account with address ${truncateEthAddress(myAccount.address)}`,
                [
                    {
                        text: "Ok",
                    },
                ]
            );
            setCreateAccountStatus(false);
        }, 500)
    }

    async function setAccountsLocal(walletBT) {
        try {
            const accounts = JSON.stringify(walletBT);
            await AsyncStorage.setItem("walletBTAccounts", accounts);
        } catch (e) {
            alert(e);
        }
    }
    useEffect(() => {
        const walletAddresses = async () => {
            try {
                const addresses = await AsyncStorage.getItem("walletBTAccounts");
                const mnemonics = await AsyncStorage.getItem("walletBTPP");
                const formattedAddresses = JSON.parse(addresses);
                setWalletBTAccounts(formattedAddresses);
                dispatch(setWalletBT(formattedAddresses));
                setWalletBTPP(mnemonics);
                console.log(addresses)
                setIsLoading(false);
            } catch (error) {
                alert(error);
            }
        };
        walletAddresses();
        if (currentAccountPK) {
            try {
                const provider = new ethers.providers.JsonRpcProvider(
                    INFURA_SEPOLIA_API
                );
                const wallet = new ethers.Wallet(currentAccountPK, provider);
                // Async call to retrieve balance
                wallet
                    .getBalance()
                    .then((balance) => {
                        setBalance(ethers.utils.formatEther(balance));
                    })
                    .catch((e) => {
                        alert(e);
                    });
            } catch (error) {
                alert(error);
            }
        }
    }, [currentAccountAddress, balance]);
    useEffect(() => {
        if (!isLoading) {
            setCurrentAccountAddress(walletBTAccounts.walletBTAccounts[0].name);
            dispatch(setAddress(walletBTAccounts.walletBTAccounts[0].name));

            setCurrentAccountPK(walletBTAccounts.walletBTAccounts[0].key);
            dispatch(setPK(walletBTAccounts.walletBTAccounts[0].key));
        }
    }, [isLoading]);
    //When you add account we want to update the accounts list
    useEffect(() => {
        const walletBTAccounts = async () => {
            try {
                const addresses = await AsyncStorage.getItem("walletBTAccounts");
                const formattedAddresses = JSON.parse(addresses);
                setWalletBTAccounts(formattedAddresses);
                dispatch(setWalletBT(formattedAddresses));
            } catch (error) {
                alert(error);
            };
            walletBTAccounts();
        }
    }, [createAccountStatus]);

    return (
        <ScrollView style={styles.rootContainer}>
            {isLoading ? (
                <ActivityIndicator size="large" color="white" />
            ) : (
                <View style={styles.bodyContainer}>
                    <Modal
                        animationType="slide"
                        onRequestClose={() => setModalStatus(false)}
                        visible={modalStatus}
                    >
                        <View style={styles.modalContainer}>
                            <FlatList
                                ListHeaderComponent={() => (
                                    <Text style={styles.modalHeader}>List of Your
                                        Accounts</Text>
                                )}
                                ListFooterComponent={() => (
                                    <Text style={styles.modalFooter}>
                                        Click on account to select it
                                    </Text>

                                )}
                                data={walletBTAccounts.walletBTAccounts}
                                renderItem={({ item }) => (
                                    <Pressable
                                        onPress={() => {
                                            setCurrentAccountAddress(item.name);
                                            setCurrentAccountPK(item.key);
                                            setModalStatus(false);
                                        }}
                                    >
                                        <Text style={styles.modalAccountNameLabel}>
                                            {item.name}
                                        </Text>
                                    </Pressable>
                                )}
                                keyExtractor={(item) => item.name}
                            />
                        </View>
                    </Modal>

                    <Modal
                        animationType="slide"
                        onRequestClose={() => setTransactionModalVisible(false)}
                        visible={transactionModalVisible}
                    >
                        <View style={styles.modalContainer}>
                            <FlatList
                                ListHeaderComponent={() => (
                                    <Text style={styles.modalHeader}>Transaction History</Text>
                                )}
                                data={transactionHistory}
                                renderItem={({ item }) => (
                                    <View style={styles.transactionItem}>
                                        <Text style={{ fontWeight: "bold" }}>Transaction ID: {item.id}</Text>
                                        <Text>Recipient Address: {item.recipient}</Text>
                                        <Text>Sent Amount: {item.amount}</Text>
                                        <Text>Date/Time: {item.date}</Text>
                                    </View>
                                )}
                                keyExtractor={(item) => item.id.toString()}
                            />
                            <Pressable
                                onPress={() => setTransactionModalVisible(false)}
                                style={styles.closeButton}
                            >
                                <Text style={{ color: "white", textAlign: "center", fontWeight: "bold", fontSize: 16, paddingVertical: 10 }}>Close</Text>
                            </Pressable>
                        </View>
                    </Modal>

                    <View style={styles.headerContainer}>
                        <Text style={styles.headerTitle}>SEPOLIA Test Network</Text>
                        <Text style={styles.headerTagLine}>
                            Currently you are using the Ethereum test network
                        </Text>
                    </View>
                    <View style={styles.body}>
                        <View>
                            <Text style={styles.label}>Current Account Address:</Text>
                            <Text style={styles.value} selectable={true}>
                                {currentAccountAddress}
                            </Text>
                        </View>
                        <View>
                            <Pressable
                                style={styles.viewTx}
                                onPress={openTransactionHistoryModal}
                            >
                                <Text
                                    style={styles.getPrivateKeyButton}>View Transactions</Text>
                            </Pressable>
                        </View>
                        <View style={styles.prvateKeyContainer}>
                            <Text style={styles.label}>
                                Click the button below to get your private key
                            </Text>
                            {!keyStatus ? (
                                <Pressable
                                    style={styles.getPrivateKey}
                                    onPress={() => setKeyStatus(true)}
                                >
                                    <Text style={styles.getPrivateKeyButton}>Get Private Key</Text>
                                </Pressable>
                            ) : (
                                <>
                                    <Text style={styles.sublabel}>{currentAccountPK}</Text>
                                    <Pressable style={styles.viewTx} onPress={() => setKeyStatus(false)}>
                                        <Text style={styles.getPrivateKeyButton}>Hide</Text>
                                    </Pressable>
                                </>
                            )}
                        </View>
                        <View style={styles.prvateKeyContainer}>
                            <Text style={styles.label}>Create New Account</Text>
                            {!createAccountStatus ? (
                                <Ionicons
                                    style={{ paddingLeft: 55 }}
                                    name="add-circle-sharp"
                                    size={40}
                                    color="#0a61e7"
                                    onPress={() => createAccount()}
                                />
                            ) : (
                                <ActivityIndicator size="large" color="#0a61e7" />
                            )}
                        </View>
                        <View style={styles.prvateKeyContainer}>
                            <Text style={styles.label}>Change the Account</Text>
                            <Pressable
                                onPress={() => setModalStatus(true)}
                                style={styles.changeAccountButton}
                            >
                                <Text style={{ paddingTop: 5, color: "white", marginRight: 4 }}>
                                    {currentAccountAddress
                                        ? truncateEthAddress(currentAccountAddress)
                                        : null}
                                </Text>
                                <Ionicons
                                    name="ios-chevron-down-circle-sharp"
                                    size={28}
                                    color="white"
                                />

                            </Pressable>
                        </View>
                        <View style={styles.footer}>
                            {balance ? (
                                <>
                                    <Text style={styles.balanceLabel}>Balance:</Text>
                                    <Text style={styles.balance}>
                                        {parseFloat(balance).toFixed(5)} SepoliaETH
                                    </Text>
                                </>
                            ) : (
                                <>
                                    <ActivityIndicator size="large" color="white" />
                                    <Text style={styles.footerBalanceLoadingText}>
                                        Loading the balance
                                    </Text>
                                </>
                            )}
                        </View>
                    </View>
                </View>
            )}
        </ScrollView>
    )
}

export default Account;

const styles = StyleSheet.create({
    rootContainer: {
        flex: 1,
    },
    headerContainer: {
        alignItems: "center",
        backgroundColor: "#0a4f95",
        paddingVertical: 20,
    },
    headerTitle: {
        fontSize: 30,
        fontWeight: "bold",

        color: "white",
        paddingVertical: 20,
    },
    headerTagLine: {
        color: "white",
        fontSize: 17,
    },
    body: {
        flex: 1,
        justifyContent: "space-between",
        // paddingVertical: 20,
    },
    label: {
        fontSize: 18,
        fontWeight: "bold",
        paddingVertical: 10,
        paddingLeft: 20,
    },
    sublabel: {
        fontSize: 14,
        paddingVertical: 10,
        paddingLeft: 20,
        color: "#40414f"
    },
    value: {
        fontSize: 14,
        paddingVertical: 5,
        paddingLeft: 20,
        color: "#40414f"
    },
    bodyContainer: {
        flex: 1,
        justifyContent: "space-between",
    },
    footer: {
        backgroundColor: "#31689f",
        paddingVertical: 25,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 55
    },
    footerBalanceLoadingText: {
        color: "white",
        fontSize: 16,
    },
    prvateKeyContainer: {
        // alignItems: "center",
        // paddingLeft: 5,
        paddingRight: 25,
        paddingTop: 20
    },
    balance: {
        color: "white",
        fontSize: 24,
        textAlign: "center",
        fontWeight: "bold",
    },
    balanceLabel: {
        fontSize: 16,
        color: "white",
    },
    viewTx: {
        borderRadius: 5,
        width: 150,
        backgroundColor: "#0a61e7",
        paddingVertical: 10,
        paddingHorizontal: 20,
        marginTop: 10,
        marginLeft: 20
        // alignSelf: "center",
    },
    getPrivateKey: {
        borderRadius: 5,
        backgroundColor: "#0a61e7",
        paddingVertical: 10,
        paddingHorizontal: 20,
        width: 150,
        marginLeft: 20
        // alignSelf: "center",
    },
    getPrivateKeyButton: {
        color: "white",
    },
    modalContainer: {
        flex: 1,
        backgroundColor: "#a4c5e6",
        justifyContent: "center",
        alignItems: "center",
    },
    changeAccountButton: {
        backgroundColor: "#0a61e7",
        flexDirection: "row",
        borderRadius: 10,
        marginLeft: 20,
        paddingHorizontal: 10,
        paddingVertical: 5,
        width: 115
    },
    modalAccountNameLabel: {
        marginVertical: 5,
        backgroundColor: "#0a4f95",
        color: "white",
        paddingVertical: 10,
        paddingHorizontal: 5,
        borderRadius: 5
    },
    modalHeader: {
        fontSize: 20,
        fontWeight: "bold",
        paddingTop: 20,
        paddingBottom: 10,
        alignSelf: "center",
    },
    modalFooter: {
        color: "grey",
        fontWeight: "bold",
        marginTop: 10,
        fontSize: 16,
        backgroundColor: "#dfe8f0",
        paddingVertical: 20,
        textAlign: "center",
    },
    closeButton: {
        padding: 10,
        backgroundColor: "#0a61e7",
        color: "white",
        width: "100%",
    },
    transactionItem: {
        width: 320,
        display: "flex",
        gap: 5,
        marginBottom: 25
    }
});